package com.zenika.pa.blog.Projet.blog.Zenika.service;

import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
public class IdGenerator {

    public String generateNewId() {
        return UUID.randomUUID().toString();
    }

}