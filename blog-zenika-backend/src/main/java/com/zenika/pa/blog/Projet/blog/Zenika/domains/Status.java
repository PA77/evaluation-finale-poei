package com.zenika.pa.blog.Projet.blog.Zenika.domains;

public enum Status {
    DRAFT(1),
    PUBLISHED(2);

    private final int statusCode;

    Status(int statusCode) {
        this.statusCode = statusCode;
    }

    public int getStatusCode() {
        return statusCode;
    }


}
