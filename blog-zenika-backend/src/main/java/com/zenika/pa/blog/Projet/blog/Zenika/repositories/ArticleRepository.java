package com.zenika.pa.blog.Projet.blog.Zenika.repositories;

import com.zenika.pa.blog.Projet.blog.Zenika.domains.Article;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface ArticleRepository extends CrudRepository<Article, String> {

}
