package com.zenika.pa.blog.Projet.blog.Zenika.contrloller.representations.users;

import com.zenika.pa.blog.Projet.blog.Zenika.contrloller.representations.DeleteRepresentationResult;
import com.zenika.pa.blog.Projet.blog.Zenika.contrloller.representations.articles.ArticleRepresentationInUser;
import com.zenika.pa.blog.Projet.blog.Zenika.contrloller.representations.articles.ArticleRepresentationInUserMapper;
import com.zenika.pa.blog.Projet.blog.Zenika.domains.Article;
import com.zenika.pa.blog.Projet.blog.Zenika.domains.User;
import com.zenika.pa.blog.Projet.blog.Zenika.service.UserService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/users")
public class UserController {

    private UserService userService;
    private UserRepresentationMapper mapper;
    private ArticleRepresentationInUserMapper mapper2;

    public UserController(UserService userService, UserRepresentationMapper mapper, ArticleRepresentationInUserMapper mapper2){
        this.userService = userService;
        this.mapper = mapper;
        this.mapper2 = mapper2;
    }

    @PostMapping
    public NewUserRepresentation createUser(@RequestBody CreationUserRepresentation body){
        User u = this.userService.createNewUser(body);
        List<ArticleRepresentationInUser> list = new ArrayList<>();
        for (Article a: u.getArticles()) {
            list.add(this.mapper2.mapToDisplayableArticleInUser(a));
        }
        return new NewUserRepresentation(u.getId(), u.getPseudo(), u.getMail(), u.getRole(), list);
    }

    @GetMapping
    List<NewUserRepresentation> getAllUsers(){
        return this.userService.getAllUsers().stream()
                .map(this.mapper:: mapToDisplayableUser)
                .collect(Collectors.toList());
    }

    @GetMapping("/{id}")
    ResponseEntity<NewUserRepresentation> getOneArticle(@PathVariable("id") String id){
        Optional<User> optionalUser = this.userService.getOneById(id);

        return optionalUser
                .map(this.mapper:: mapToDisplayableUser)
                .map(ResponseEntity::ok)
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    @DeleteMapping("/{id}")
    public DeleteRepresentationResult deleteAUserById(@PathVariable(value = "id") String id){
        boolean result = this.userService.deleteAUser(id);
        return new DeleteRepresentationResult(result);
    }


}
