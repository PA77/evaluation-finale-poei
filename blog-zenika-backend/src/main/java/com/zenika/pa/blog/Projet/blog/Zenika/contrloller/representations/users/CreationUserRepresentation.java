package com.zenika.pa.blog.Projet.blog.Zenika.contrloller.representations.users;

public class CreationUserRepresentation {

    private String pseudo;
    private String mail;
    private String role;

    public String getPseudo() {
        return pseudo;
    }

    public void setPseudo(String pseudo) {
        this.pseudo = pseudo;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }
}
