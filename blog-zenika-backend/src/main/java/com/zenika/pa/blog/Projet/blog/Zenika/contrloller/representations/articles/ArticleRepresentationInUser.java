package com.zenika.pa.blog.Projet.blog.Zenika.contrloller.representations.articles;

public class ArticleRepresentationInUser {

    private String id;
    private String title;
    private String content;
    private String resume;
    private String status;

    public ArticleRepresentationInUser(String id, String title, String content, String resume, String status){
        this.id = id;
        this.title = title;
        this.content = content;
        this.resume = resume;
        this.status = status;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getResume() {
        return resume;
    }

    public void setResume(String resume) {
        this.resume = resume;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
