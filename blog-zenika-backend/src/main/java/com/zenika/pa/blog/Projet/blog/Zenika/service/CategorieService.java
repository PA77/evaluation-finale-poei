package com.zenika.pa.blog.Projet.blog.Zenika.service;

import com.zenika.pa.blog.Projet.blog.Zenika.domains.Categorie;
import com.zenika.pa.blog.Projet.blog.Zenika.repositories.CategorieRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import java.util.List;
import java.util.Optional;

@Component
public class CategorieService {

    private CategorieRepository categorieRepository;
    private IdGenerator idGenerator;

    @Autowired
    public CategorieService(CategorieRepository categorieRepository, IdGenerator idGenerator){
        this.categorieRepository = categorieRepository;
        this.idGenerator = idGenerator;
    }

    public List<Categorie> getAllCategories() {
        return (List<Categorie>) this.categorieRepository.findAll();
    }

    public Optional<Categorie> getOneById(String id) {
        return this.categorieRepository.findById(id);
    }

    public boolean deleteACategorie(String id) {
        if (this.categorieRepository.findById(id).isPresent()) {
            this.categorieRepository.deleteById(id);
            return true;
        } else {
            return false;
        }
    }

    }

