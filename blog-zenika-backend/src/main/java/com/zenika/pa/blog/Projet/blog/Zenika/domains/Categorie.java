package com.zenika.pa.blog.Projet.blog.Zenika.domains;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "categories")
public class Categorie {

    @Id
    private String id;
    private String name;

    @OneToMany(mappedBy = "categorie", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Article> articles;

    protected Categorie(){}

    public Categorie(String id, String name, List<Article> articles){
        this.id = id;
        this.name = name;
        this.articles = articles;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Article> getArticles() {
        return articles;
    }

    public void setArticles(List<Article> articles) {
        this.articles = articles;
    }
}
