package com.zenika.pa.blog.Projet.blog.Zenika.contrloller.representations.articles;

public class UpdatableArticleRepresentation {
    private String title;
    private String content;
    private String resume;

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getResume() {
        return resume;
    }

    public void setResume(String resume) {
        this.resume = resume;
    }
}
