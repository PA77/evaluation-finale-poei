package com.zenika.pa.blog.Projet.blog.Zenika.contrloller.representations.users;

import com.zenika.pa.blog.Projet.blog.Zenika.contrloller.representations.articles.NewArticleRepresentation;
import com.zenika.pa.blog.Projet.blog.Zenika.domains.User;
import org.springframework.stereotype.Component;

@Component
public class UserRepresentationinArticleMapper {

    public UserRepresentationInArticle mapToDisplayableUserInArticle(User user) {

        UserRepresentationInArticle result = new UserRepresentationInArticle(user.getId(),
                user.getPseudo(), user.getMail(), user.getRole());

        return result;

    }
}
