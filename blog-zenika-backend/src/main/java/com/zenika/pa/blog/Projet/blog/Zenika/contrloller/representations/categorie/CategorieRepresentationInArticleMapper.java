package com.zenika.pa.blog.Projet.blog.Zenika.contrloller.representations.categorie;

import com.zenika.pa.blog.Projet.blog.Zenika.domains.Categorie;
import org.springframework.stereotype.Component;

@Component
public class CategorieRepresentationInArticleMapper {

    public CategorieRepresentationInArticle mapToDisplayableCategorieInArticle(Categorie categorie) {

        CategorieRepresentationInArticle result = new CategorieRepresentationInArticle(categorie.getId(),
                categorie.getName());
        return result;
    }
}
