package com.zenika.pa.blog.Projet.blog.Zenika.repositories;

import com.zenika.pa.blog.Projet.blog.Zenika.domains.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends CrudRepository<User, String> {

    //@Query("select User from Users where pseudo = ?1")
    Optional<User> findByPseudo(String pseudo);
}
