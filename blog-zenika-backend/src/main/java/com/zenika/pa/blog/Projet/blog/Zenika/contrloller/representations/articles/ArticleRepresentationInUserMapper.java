package com.zenika.pa.blog.Projet.blog.Zenika.contrloller.representations.articles;

import com.zenika.pa.blog.Projet.blog.Zenika.domains.Article;
import org.springframework.stereotype.Component;

@Component
public class ArticleRepresentationInUserMapper {

    public ArticleRepresentationInUser mapToDisplayableArticleInUser(Article article) {
        ArticleRepresentationInUser result = new ArticleRepresentationInUser(article.getId(),
                article.getTitle(), article.getContent(), article.getResume(), article.getStatus());
        return result;
    }
}
