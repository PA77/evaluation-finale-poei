package com.zenika.pa.blog.Projet.blog.Zenika.contrloller.representations.categorie;

import com.zenika.pa.blog.Projet.blog.Zenika.service.CategorieService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("categories")
public class CategorieController {

    private CategorieService categorieService;
    private CategorieRepresentationInArticleMapper mapper;

    public CategorieController(CategorieService categorieService, CategorieRepresentationInArticleMapper mapper) {
        this.categorieService = categorieService;
        this.mapper = mapper;
    }

    @GetMapping
    List<CategorieRepresentationInArticle> getAllCategories(){
        return this.categorieService.getAllCategories().stream()
                .map(this.mapper:: mapToDisplayableCategorieInArticle)
                .collect(Collectors.toList());
    }

}
