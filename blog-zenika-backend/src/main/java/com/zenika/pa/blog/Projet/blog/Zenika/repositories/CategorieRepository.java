package com.zenika.pa.blog.Projet.blog.Zenika.repositories;

import com.zenika.pa.blog.Projet.blog.Zenika.domains.Categorie;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CategorieRepository extends CrudRepository<Categorie, String> {

    //@Query("select categorie from categories where name = ?1")
    Categorie findByName(String name);
}
