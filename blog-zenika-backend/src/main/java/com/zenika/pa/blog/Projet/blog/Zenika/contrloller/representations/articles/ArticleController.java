package com.zenika.pa.blog.Projet.blog.Zenika.contrloller.representations.articles;

import com.zenika.pa.blog.Projet.blog.Zenika.contrloller.representations.*;
import com.zenika.pa.blog.Projet.blog.Zenika.contrloller.representations.categorie.CategorieRepresentationInArticle;
import com.zenika.pa.blog.Projet.blog.Zenika.contrloller.representations.categorie.CategorieRepresentationInArticleMapper;
import com.zenika.pa.blog.Projet.blog.Zenika.contrloller.representations.users.UserRepresentationInArticle;
import com.zenika.pa.blog.Projet.blog.Zenika.contrloller.representations.users.UserRepresentationMapper;
import com.zenika.pa.blog.Projet.blog.Zenika.contrloller.representations.users.UserRepresentationinArticleMapper;
import com.zenika.pa.blog.Projet.blog.Zenika.domains.Article;
import com.zenika.pa.blog.Projet.blog.Zenika.service.ArticleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/articles")
public class ArticleController {

    ArticleService articleService;
    ArticleRepresentationMapper mapper;
    UserRepresentationMapper mapper2;
    UserRepresentationinArticleMapper mapper3;
    CategorieRepresentationInArticleMapper mapper4;

    @Autowired
    public ArticleController(ArticleService articleService, ArticleRepresentationMapper mapper,
                             UserRepresentationMapper mapper2, UserRepresentationinArticleMapper mapper3,
                             CategorieRepresentationInArticleMapper mapper4){
        this.articleService = articleService;
        this.mapper = mapper;
        this.mapper2 = mapper2;
        this.mapper3 = mapper3;
        this.mapper4 = mapper4;
    }

    @GetMapping
    List<NewArticleRepresentation>getAllArticles(){
        return this.articleService.getAllArticles().stream()
                .map(this.mapper:: mapToDisplayableArticle)
                .collect(Collectors.toList());
    }

    @GetMapping("/{id}")
    ResponseEntity<NewArticleRepresentation> getOneArticle(@PathVariable("id") String id){
        Optional<Article> optionalArticle = this.articleService.getOneById(id);

        return optionalArticle
                .map(this.mapper::mapToDisplayableArticle)
                .map(ResponseEntity::ok)
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    @DeleteMapping("/{id}")
    public DeleteRepresentationResult deleteAnArticleById(@PathVariable(value = "id") String id){
        boolean result = this.articleService.deleteAnArticle(id);
        return new DeleteRepresentationResult(result);
    }

    @PostMapping
    public NewArticleRepresentation createArticle(@RequestBody CreationArticleRepresentation body){
        Article a = this.articleService.createNewArticle(body);
        UserRepresentationInArticle u = this.mapper3.mapToDisplayableUserInArticle(a.getAuthor());
        CategorieRepresentationInArticle c = this.mapper4.mapToDisplayableCategorieInArticle(a.getCategorie());

        return new NewArticleRepresentation(a.getId(), a.getTitle(), a.getContent(), a.getResume(), a.getStatus(), u, c);
    }

    @PutMapping("/{id}")
    public ResponseEntity<NewArticleRepresentation> modifyArticle(@PathVariable("id") String id, @RequestBody UpdatableArticleRepresentation
            body) {
        this.articleService.updateAnArticle(id, body);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @GetMapping("/draft")
    List<NewArticleRepresentation>getAllDraftArticles(){
        return this.articleService.getDraftArticles().stream()
                .map(this.mapper:: mapToDisplayableArticle)
                .collect(Collectors.toList());
    }

    @PutMapping("/publish/{id}")
    public ResponseEntity<NewArticleRepresentation> publishArticle(@PathVariable("id") String id){
        this.articleService.changeArticleStatus(id);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

}
