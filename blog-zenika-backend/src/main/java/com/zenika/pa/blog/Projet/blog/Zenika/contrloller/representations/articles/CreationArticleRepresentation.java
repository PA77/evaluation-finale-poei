package com.zenika.pa.blog.Projet.blog.Zenika.contrloller.representations.articles;

import com.zenika.pa.blog.Projet.blog.Zenika.contrloller.representations.categorie.CategorieRepresentationInArticle;
import com.zenika.pa.blog.Projet.blog.Zenika.contrloller.representations.users.UserRepresentationInArticle;

public class CreationArticleRepresentation {

    private String title;
    private String content;
    private String resume;
    private UserRepresentationInArticle author;
    private CategorieRepresentationInArticle cat;


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getResume() {
        return resume;
    }

    public void setResume(String resume) {
        this.resume = resume;
    }

    public UserRepresentationInArticle getAuthor() {
        return author;
    }

    public void setAuthor(UserRepresentationInArticle author) {
        this.author = author;
    }

    public CategorieRepresentationInArticle getCat() {
        return cat;
    }

    public void setCat(CategorieRepresentationInArticle cat) {
        this.cat = cat;
    }
}
