package com.zenika.pa.blog.Projet.blog.Zenika.contrloller.representations.users;

import com.zenika.pa.blog.Projet.blog.Zenika.contrloller.representations.articles.ArticleRepresentationInUser;
import com.zenika.pa.blog.Projet.blog.Zenika.contrloller.representations.articles.ArticleRepresentationInUserMapper;
import com.zenika.pa.blog.Projet.blog.Zenika.domains.Article;
import com.zenika.pa.blog.Projet.blog.Zenika.domains.User;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class UserRepresentationMapper {

    private ArticleRepresentationInUserMapper mapper;

    public UserRepresentationMapper(ArticleRepresentationInUserMapper mapper) {
        this.mapper = mapper;
    }

    public NewUserRepresentation mapToDisplayableUser(User user) {
        List<ArticleRepresentationInUser> newList = new ArrayList<>();
        for (Article a: user.getArticles()) {
            newList.add(this.mapper.mapToDisplayableArticleInUser(a));
        }
        NewUserRepresentation result = new NewUserRepresentation(user.getId(),
                user.getPseudo(), user.getMail(), user.getRole(), newList);

        return result;

    }
}
