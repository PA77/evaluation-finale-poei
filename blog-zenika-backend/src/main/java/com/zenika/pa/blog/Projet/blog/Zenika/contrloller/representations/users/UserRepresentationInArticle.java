package com.zenika.pa.blog.Projet.blog.Zenika.contrloller.representations.users;

public class UserRepresentationInArticle {
    private String id;
    private String pseudo;
    private String mail;
    private String role;

    public UserRepresentationInArticle(String id,String pseudo, String mail, String role){
        this.id = id;
        this.pseudo = pseudo;
        this.mail = mail;
        this.role = role;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPseudo() {
        return pseudo;
    }

    public void setPseudo(String pseudo) {
        this.pseudo = pseudo;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }
}
