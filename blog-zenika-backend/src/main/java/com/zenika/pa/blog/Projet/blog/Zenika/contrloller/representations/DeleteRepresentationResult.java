package com.zenika.pa.blog.Projet.blog.Zenika.contrloller.representations;

public class DeleteRepresentationResult {
    boolean result = true;

    public boolean isResult() {
        return result;
    }

    public void setResult(boolean result) {
        this.result = result;
    }

    public DeleteRepresentationResult(boolean result){
        this.result = result;
    }
}
