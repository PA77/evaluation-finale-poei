package com.zenika.pa.blog.Projet.blog.Zenika.contrloller.representations.articles;

import com.zenika.pa.blog.Projet.blog.Zenika.contrloller.representations.categorie.CategorieRepresentationInArticle;
import com.zenika.pa.blog.Projet.blog.Zenika.contrloller.representations.categorie.CategorieRepresentationInArticleMapper;
import com.zenika.pa.blog.Projet.blog.Zenika.contrloller.representations.users.UserRepresentationinArticleMapper;
import com.zenika.pa.blog.Projet.blog.Zenika.domains.Article;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ArticleRepresentationMapper {

    private UserRepresentationinArticleMapper mapper;
    private CategorieRepresentationInArticleMapper cmapp;

    @Autowired
    public ArticleRepresentationMapper(UserRepresentationinArticleMapper mapper,
                                       CategorieRepresentationInArticleMapper cmapp) {
        this.mapper = mapper;
        this.cmapp = cmapp;
    }

    public NewArticleRepresentation mapToDisplayableArticle(Article article) {
        CategorieRepresentationInArticle c = this.cmapp.mapToDisplayableCategorieInArticle(article.getCategorie());
            NewArticleRepresentation result = new NewArticleRepresentation(article.getId(),
                    article.getTitle(), article.getContent(), article.getResume(), article.getStatus(),
                    this.mapper.mapToDisplayableUserInArticle(article.getAuthor()), c);
            return result;
    }

}
