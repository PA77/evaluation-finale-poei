package com.zenika.pa.blog.Projet.blog.Zenika.service;

import com.zenika.pa.blog.Projet.blog.Zenika.contrloller.representations.articles.CreationArticleRepresentation;
import com.zenika.pa.blog.Projet.blog.Zenika.contrloller.representations.articles.UpdatableArticleRepresentation;
import com.zenika.pa.blog.Projet.blog.Zenika.domains.Article;
import com.zenika.pa.blog.Projet.blog.Zenika.domains.Categorie;
import com.zenika.pa.blog.Projet.blog.Zenika.domains.User;
import com.zenika.pa.blog.Projet.blog.Zenika.repositories.ArticleRepository;
import com.zenika.pa.blog.Projet.blog.Zenika.repositories.CategorieRepository;
import com.zenika.pa.blog.Projet.blog.Zenika.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Component
public class ArticleService {

    private ArticleRepository articleRepository;
    private IdGenerator idGenerator;
    private UserRepository userRepository;
    private CategorieRepository categorieRepository;

    @Autowired
    public ArticleService(ArticleRepository articleRepository, IdGenerator idGenerator, UserRepository userRepository,
                          CategorieRepository categorieRepository) {
        this.articleRepository = articleRepository;
        this.idGenerator = idGenerator;
        this.userRepository = userRepository;
        this.categorieRepository = categorieRepository;
    }

    public List<Article> getAllArticles() {
        return (List<Article>) this.articleRepository.findAll();
    }

    public Optional<Article> getOneById(String id) {
        return this.articleRepository.findById(id);
    }

    public boolean deleteAnArticle(String id) {
        if (this.articleRepository.findById(id).isPresent()) {
            this.articleRepository.deleteById(id);
            return true;
        } else {
            return false;
        }
    }

    @Transactional(rollbackOn = Exception.class)
    public Article createNewArticle(CreationArticleRepresentation a) {
        String articleId = this.idGenerator.generateNewId();
        Optional<User> optionalUser = this.userRepository.findByPseudo(a.getAuthor().getPseudo());
        Categorie c = this.categorieRepository.findByName(a.getCat().getName());

        if (optionalUser.isPresent()) {
            Article article1 = new Article(articleId, a.getTitle(), a.getContent(), a.getResume(), optionalUser.get(), c);
            return this.articleRepository.save(article1);
        } else {
            Article article = new Article(articleId, a.getTitle(), a.getContent(), a.getResume(),
                    this.userRepository.findByPseudo("Anonymous").get(), c);
            return this.articleRepository.save(article);
        }
    }

    public void updateAnArticle(String id, UpdatableArticleRepresentation u) {
        String content = u.getContent();
        String title = u.getTitle();
        String resume = u.getResume();

        Optional<Article> optionalArticle = this.articleRepository.findById(id);
        optionalArticle.map(r -> {
                    if (r.getStatus().equals("PUBLISHED")) {
                        if (content != null) {
                            r.setContent(content);
                        }
                        this.articleRepository.save(r);
                        return r;
                    } else {
                        if (content != null) {
                            r.setContent(content);
                        }
                        if (title != null) {
                            r.setTitle(title);
                        }
                        if (resume != null) {
                            r.setResume(resume);
                        }
                        this.articleRepository.save(r);
                        return r;
                    }
                }
        );
    }

    public List<Article> getDraftArticles() {
        List<Article> articles = (List<Article>) this.articleRepository.findAll();
        List<Article> draftArticles = new ArrayList<>();

        for (Article a : articles) {
            if (a.getStatus().equals("DRAFT")) {
                draftArticles.add(a);
            }
        }
        return draftArticles;
    }

    public void changeArticleStatus(String id) {
        Optional<Article> optionalArticle = this.articleRepository.findById(id);
        optionalArticle.map(result -> {
            if (result.getStatus().equals("DRAFT")) {
                result.setStatus("PUBLISHED");
            }
            this.articleRepository.save(result);
            return result;
        });
    }
}
