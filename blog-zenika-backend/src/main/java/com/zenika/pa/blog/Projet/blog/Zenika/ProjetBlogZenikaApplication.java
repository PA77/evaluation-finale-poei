package com.zenika.pa.blog.Projet.blog.Zenika;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProjetBlogZenikaApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProjetBlogZenikaApplication.class, args);
	}

}
