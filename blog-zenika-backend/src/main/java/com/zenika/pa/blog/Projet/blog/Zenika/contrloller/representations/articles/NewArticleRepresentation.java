package com.zenika.pa.blog.Projet.blog.Zenika.contrloller.representations.articles;

import com.zenika.pa.blog.Projet.blog.Zenika.contrloller.representations.categorie.CategorieRepresentationInArticle;
import com.zenika.pa.blog.Projet.blog.Zenika.contrloller.representations.users.NewUserRepresentation;
import com.zenika.pa.blog.Projet.blog.Zenika.contrloller.representations.users.UserRepresentationInArticle;

public class NewArticleRepresentation {

    private String id;
    private String title;
    private String content;
    private String resume;
    private String status;
    private UserRepresentationInArticle author;
    private CategorieRepresentationInArticle cat;


    public NewArticleRepresentation(String id, String title, String content, String resume, String status,
                                    UserRepresentationInArticle author, CategorieRepresentationInArticle cat){
        this.id = id;
        this.title = title;
        this.content = content;
        this.resume = resume;
        this.status = status;
        this.author = author;
        this.cat = cat;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getResume() {
        return resume;
    }

    public void setResume(String resume) {
        this.resume = resume;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public UserRepresentationInArticle getAuthor() {
        return author;
    }

    public void setAuthor(UserRepresentationInArticle author) {
        this.author = author;
    }

    public CategorieRepresentationInArticle getCat() {
        return cat;
    }

    public void setCat(CategorieRepresentationInArticle cat) {
        this.cat = cat;
    }
}
