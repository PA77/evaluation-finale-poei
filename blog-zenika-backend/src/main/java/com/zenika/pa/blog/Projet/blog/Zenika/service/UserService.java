package com.zenika.pa.blog.Projet.blog.Zenika.service;

import com.zenika.pa.blog.Projet.blog.Zenika.contrloller.representations.users.CreationUserRepresentation;
import com.zenika.pa.blog.Projet.blog.Zenika.domains.Article;
import com.zenika.pa.blog.Projet.blog.Zenika.domains.User;
import com.zenika.pa.blog.Projet.blog.Zenika.repositories.ArticleRepository;
import com.zenika.pa.blog.Projet.blog.Zenika.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Component
public class UserService {

    private UserRepository userRepository;
    private IdGenerator idGenerator;
    private ArticleRepository articleRepository;

    @Autowired
    public UserService(UserRepository userRepository, IdGenerator idGenerator, ArticleRepository articleRepository){
        this.userRepository = userRepository;
        this.idGenerator = idGenerator;
        this.articleRepository = articleRepository;
    }

    @Transactional(rollbackOn = Exception.class)
    public User createNewUser(CreationUserRepresentation userRepresentation) {
        if (this.userRepository.findByPseudo(userRepresentation.getPseudo()).isEmpty()){
        String userId = this.idGenerator.generateNewId();
        List<Article> articles = new ArrayList<>();
        User user = new User(userId, userRepresentation.getPseudo(), userRepresentation.getMail(),
                userRepresentation.getRole(), articles);
        return this.userRepository.save(user);
        } else {
            List<Article> articles = new ArrayList<>();
            User u = new User(this.idGenerator.generateNewId(), "Anonymous", "anonymous@anonymous", "DEFAULT",
                    articles);
            return u;
        }
    }

    public List<User> getAllUsers() {
        return (List<User>) this.userRepository.findAll();
    }

    public Optional<User> getOneById(String id) {
        return this.userRepository.findById(id);
    }

    public boolean deleteAUser(String id) {
        if (this.userRepository.findById(id).isPresent()) {
            this.userRepository.deleteById(id);
            return true;
        } else {
            return false;
        }
    }


}
