package com.zenika.pa.blog.Projet.blog.Zenika.contrloller.representations.users;

import com.zenika.pa.blog.Projet.blog.Zenika.contrloller.representations.articles.ArticleRepresentationInUser;
import com.zenika.pa.blog.Projet.blog.Zenika.domains.Article;

import java.util.List;

public class NewUserRepresentation {

    private String id;
    private String pseudo;
    private String mail;
    private String role;
    private List<ArticleRepresentationInUser> articles;

    public NewUserRepresentation(String id, String pseudo, String mail, String role, List<ArticleRepresentationInUser> articles){
        this.id = id;
        this.pseudo = pseudo;
        this.mail = mail;
        this.role = role;
        this.articles = articles;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPseudo() {
        return pseudo;
    }

    public void setPseudo(String pseudo) {
        this.pseudo = pseudo;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public List<ArticleRepresentationInUser> getArticles() {
        return articles;
    }

    public void setArticles(List<ArticleRepresentationInUser> articles) {
        this.articles = articles;
    }
}
