package com.zenika.pa.blog.Projet.blog.Zenika.contrloller.representations.categorie;

public class CategorieRepresentationInArticle {

    private String id;
    private String name;

    public CategorieRepresentationInArticle(String id, String name){
        this.id = id;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
