drop table if exists users;
create table users (
id text primary key,
pseudo text not null,
mail text not null,
role text not null
);

drop table if exists categories;
create table categories (
id text primary key,
name text
)

drop table if exists articles;
create table articles (
id text primary key,
title text not null,
content text not null,
resume text not null,
status text not null,
user_id text,
categorie_id text,
FOREIGN KEY (user_id) references users (id),
FOREIGN KEY (categorie_id) references categories (id)
);

insert into categories (id, name) values ('fklfkfkfkjfkjfkjf', 'Sport');
insert into categories (id, name) values ('dh5566jdhfl', 'Musique');
insert into categories (id, name) values ('lkdjk345jdhfl', 'Informatique');
insert into categories (id, name) values ('odokd87ç', 'Cinéma');
insert into categories (id, name) values ('fkgfkj778kfpoo', 'Divers');