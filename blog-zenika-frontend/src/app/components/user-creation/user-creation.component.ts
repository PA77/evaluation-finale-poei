import { Component, OnInit } from '@angular/core';
import {UserCreate} from '../../models/user-create';
import {UserService} from '../../service/user.service';

@Component({
  selector: 'app-user-creation',
  templateUrl: './user-creation.component.html',
  styleUrls: ['./user-creation.component.css']
})
export class UserCreationComponent implements OnInit {

  userCreate: UserCreate;
  pseudo: string;
  mail: string;
  role: string;
  creationOk = false;

  constructor(private userService: UserService) { }

  ngOnInit() {
  }

  save(){
    this.userCreate = new UserCreate(this.pseudo, this.mail, this.role);
    this.userService.createNewUser(this.userCreate).subscribe(() =>
    this.creationOk = true);
  }

}
