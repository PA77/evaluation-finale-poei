import { Component, OnInit } from '@angular/core';
import {ArticleCreate} from '../../models/article-create';
import {ArticleService} from '../../service/article.service';
import {User} from '../../models/user';
import {Categorie} from '../../models/categorie';

@Component({
  selector: 'app-create-article',
  templateUrl: './create-article.component.html',
  styleUrls: ['./create-article.component.css']
})
export class CreateArticleComponent implements OnInit {

  articleCreate: ArticleCreate;
  title: string;
  content: string;
  resume: string;
  creationOk = false;
  author: User;
  pseudo: string;
  cat: Categorie;
  categorieName: string;

  constructor(private articleService: ArticleService) { }

  ngOnInit() {
  }

  save() {
    this.cat = new Categorie('', this.categorieName);
    this.author = new User('', this.pseudo, '', '');
    this.articleCreate = new ArticleCreate(this.title, this.content, this.resume, this.author, this.cat);
    this.articleService.createNewArticle(this.articleCreate).subscribe(() =>
    this.creationOk = true);
    this.title = '';
    this.resume = '';
    this.content = '';
    this.pseudo = '';
    this.categorieName = '';
  }

}
