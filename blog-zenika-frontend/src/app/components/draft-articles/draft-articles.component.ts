import { Component, OnInit } from '@angular/core';
import {Article} from '../../models/article';
import {ArticleService} from '../../service/article.service';

@Component({
  selector: 'app-draft-articles',
  templateUrl: './draft-articles.component.html',
  styleUrls: ['./draft-articles.component.css']
})
export class DraftArticlesComponent implements OnInit {

  articles: Article[];

  constructor(private articleService: ArticleService) { }

  ngOnInit() {
    this.articleService.getAllDraftArticles().subscribe(a =>
    this.articles = a);
  }

}
