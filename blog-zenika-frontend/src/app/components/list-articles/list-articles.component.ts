import { Component, OnInit } from '@angular/core';
import {Article} from '../../models/article';
import {ArticleService} from '../../service/article.service';

@Component({
  selector: 'app-list-articles',
  templateUrl: './list-articles.component.html',
  styleUrls: ['./list-articles.component.css']
})
export class ListArticlesComponent implements OnInit {

  articles: Article[];
  term;
  catValue = '';

  constructor(private articleService: ArticleService) { }

  ngOnInit() {
    this.articleService.getArticles().subscribe(articlesResult =>
      this.articles = articlesResult
    );
  }

  getArticles() {
    if (this.catValue === 'Sport' || this.catValue === 'Cinéma' || this.catValue === 'Musique' || this.catValue === 'Informatique'
    || this.catValue === 'Divers') {
      return this.articles.filter(article => article.cat.name === this.catValue);
    } else {
      return this.articles;
    }
  }

  setCatValue(value: string) {
        this.catValue = value;
  }
}
