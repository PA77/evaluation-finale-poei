import { Component, OnInit } from '@angular/core';
import {ArticleService} from '../../service/article.service';
import {ActivatedRoute} from '@angular/router';
import {Article} from '../../models/article';
import {EmptyBody} from '../../models/empty-body';

@Component({
  selector: 'app-article-details',
  templateUrl: './article-details.component.html',
  styleUrls: ['./article-details.component.css']
})
export class ArticleDetailsComponent implements OnInit {

  article: Article;
  id: string;
  emptyBody: EmptyBody;
  isPublish = false;

  constructor(private articleService: ArticleService, private route: ActivatedRoute) { }

  ngOnInit() {
    this.id = this.route.snapshot.paramMap.get('id');
    this.articleService.getOneArticle(this.id).subscribe(result => {
      this.article = result;
    });

  }

  publishAnArticle() {
    this.articleService.publishNewArticle(this.id, this.emptyBody).subscribe(result =>
    this.isPublish = true);
  }

}
