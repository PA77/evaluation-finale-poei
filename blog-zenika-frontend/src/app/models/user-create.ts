export class UserCreate {
  pseudo: string;
  mail: string;
  role: string;

  constructor(pseudo: string, mail: string, role: string) {
    this.pseudo = pseudo;
    this.mail = mail;
    this.role = role;
  }
}
