import {User} from './user';
import {Categorie} from './categorie';

export class Article {

  id: string;
  title: string;
  content: string;
  resume: string;
  status: string;
  author: User;
  cat: Categorie;

  constructor(id: string, title: string, content: string, resume: string, author: User, cat: Categorie) {
    this.id = id;
    this.title = title;
    this.content = content;
    this.resume = resume;
    this.cat = cat;
  }
}
