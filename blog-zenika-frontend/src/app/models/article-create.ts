import {User} from './user';
import {Categorie} from './categorie';

export class ArticleCreate {
  title: string;
  content: string;
  resume: string;
  author: User;
  cat: Categorie;

  constructor(title: string, content: string, resume: string, author: User, cat: Categorie) {
    this.title = title;
    this.content = content;
    this.resume = resume;
    this.author = author;
    this.cat = cat;
  }
}
