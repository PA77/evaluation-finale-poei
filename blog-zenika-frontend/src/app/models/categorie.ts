import {Article} from './article';

export class Categorie {

  id: string;
  name: string;
  articles: Article[];

  constructor(id: string, name: string) {
    this.id = id;
    this.name = name;
    this.articles = [];
  }
}
