import {Article} from './article';

export class User {

  id: string;
  pseudo: string;
  mail: string;
  role: string;
  articles: Article[];

  constructor(id: string, pseudo: string, mail: string, role: string) {
    this.id = id;
    this.pseudo = pseudo;
    this.mail = mail;
    this.role = role;
    this.articles = [];
  }
}
