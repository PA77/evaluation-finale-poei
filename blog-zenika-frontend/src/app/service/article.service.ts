import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Article} from '../models/article';
import {ArticleCreate} from '../models/article-create';
import {EmptyBody} from '../models/empty-body';

@Injectable()
export class ArticleService {

  constructor(private http: HttpClient) {
  }

  getArticles(): Observable<Article[]> {
    return this.http.get<Article[]>('http://localhost:8080/articles');
  }

  getOneArticle(id: string): Observable<Article> {
    return this.http.get<Article>(`http://localhost:8080/articles/${id}`);
  }

  createNewArticle(articleCreate: ArticleCreate): Observable<ArticleCreate> {
    return this.http.post<ArticleCreate>(`http://localhost:8080/articles`, articleCreate);
  }

  getAllDraftArticles(): Observable<Article[]> {
    return this.http.get<Article[]>('http://localhost:8080/articles/draft');
  }

  publishNewArticle(id: string, emptyBody: EmptyBody): Observable<Article> {
    return this.http.put<Article>(`http://localhost:8080/articles/publish/${id}`, emptyBody);
  }
}
