import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {UserCreate} from '../models/user-create';
import {Observable} from 'rxjs';

@Injectable()
export class UserService {

  constructor(private http: HttpClient) {
  }

  createNewUser(userCreate: UserCreate): Observable<UserCreate> {
    return this.http.post<UserCreate>('http://localhost:8080/users', userCreate);
  }
}
