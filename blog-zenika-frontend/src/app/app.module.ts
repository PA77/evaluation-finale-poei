import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { ArticleComponent } from './components/article/article.component';
import { ListArticlesComponent } from './components/list-articles/list-articles.component';
import {RouterModule, Routes} from '@angular/router';
import {HttpClientModule} from '@angular/common/http';
import {ArticleService} from './service/article.service';
import { ArticleDetailsComponent } from './components/article-details/article-details.component';
import { AdminGestionComponent } from './components/admin-gestion/admin-gestion.component';
import { CreateArticleComponent } from './components/create-article/create-article.component';
import {FormsModule} from '@angular/forms';
import { DraftArticlesComponent } from './components/draft-articles/draft-articles.component';
import { UserCreationComponent } from './components/user-creation/user-creation.component';
import {UserService} from './service/user.service';
import {Ng2SearchPipeModule} from 'ng2-search-filter';

const appRoutes: Routes = [
  { path: 'articles', component: ListArticlesComponent },
  { path: 'details/:id', component: ArticleDetailsComponent },
  { path: 'admin', component: AdminGestionComponent },
  { path: 'creation', component: CreateArticleComponent },
  { path: 'draft', component: DraftArticlesComponent },
  { path: 'userscreation', component: UserCreationComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    ArticleComponent,
    ListArticlesComponent,
    ArticleDetailsComponent,
    AdminGestionComponent,
    CreateArticleComponent,
    DraftArticlesComponent,
    UserCreationComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    RouterModule.forRoot(
      appRoutes,
    ),
    FormsModule,
    Ng2SearchPipeModule
  ],
  providers: [
    ArticleService,
    UserService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
